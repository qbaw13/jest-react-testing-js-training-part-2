import { ApolloClient, InMemoryCache } from '@apollo/client';

export default function createClient() {
  return new ApolloClient({
    uri: 'https://mockend.com/qbaw13/test/graphql',
    cache: new InMemoryCache(),
  });
}
