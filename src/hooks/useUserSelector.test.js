import { renderHook } from '@testing-library/react-hooks';
import { act } from 'react-dom/test-utils';
import fetchUserMock from '../api/rest/fetch-user';
import useUserSelector from './useUserSelector';

jest.mock('../api/rest/fetch-user');

const user = {
  id: 'ID',
  name: 'NAME',
  surname: 'SURNAME',
  createdAt: 'CREATED_AT',
};

describe('user selector hook', () => {
  it('should return user', async () => {
    // given
    fetchUserMock.mockReturnValueOnce(
      Promise.resolve({
        data: user,
      })
    );

    // when
    const { result, waitForNextUpdate } = renderHook(() =>
      useUserSelector('SOME_ID')
    );
    await waitForNextUpdate();

    // then
    expect(result.current.user).toMatchObject({
      id: 'ID',
      name: 'NAME',
      surname: 'SURNAME',
      createdAt: 'CREATED_AT',
    });
  });

  it('should call fetch user api with initial user id 1', async () => {
    // given
    fetchUserMock.mockReturnValueOnce(
      Promise.resolve({
        data: user,
      })
    );

    // when
    const { waitForNextUpdate } = renderHook(() => useUserSelector());
    await waitForNextUpdate();

    // then
    expect(fetchUserMock).toHaveBeenCalledWith('1');
  });

  it('should call fetch user api with given id', async () => {
    // given
    fetchUserMock.mockReturnValueOnce(
      Promise.resolve({
        data: user,
      })
    );

    // when
    const { waitForNextUpdate } = renderHook(() => useUserSelector('SOME_ID'));
    await waitForNextUpdate();

    // then
    expect(fetchUserMock).toHaveBeenCalledWith('SOME_ID');
  });

  it('should NOT call fetch user api when user id is null', async () => {
    // given
    fetchUserMock.mockReturnValueOnce(
      Promise.resolve({
        data: user,
      })
    );

    // when
    const { waitForNextUpdate } = renderHook(() => useUserSelector(null));

    // then
    try {
      await waitForNextUpdate();
    } catch (error) {
      // ignore
    } finally {
      expect(fetchUserMock).not.toHaveBeenCalled();
    }
  });

  it('should fetch random user', async () => {
    // given
    fetchUserMock.mockReturnValue(
      Promise.resolve({
        data: user,
      })
    );
    const originalMathRandomFn = Math.random;
    const stubMathRandomFn = () => 0.22;
    Math.random = stubMathRandomFn;
    const { result, waitForNextUpdate } = renderHook(() => useUserSelector());
    await waitForNextUpdate();

    // when
    act(() => {
      result.current.randomUser();
    });
    await waitForNextUpdate();

    // then
    expect(fetchUserMock).toHaveBeenLastCalledWith('22');
    Math.random = originalMathRandomFn;
  });
});
