import { useCallback, useEffect, useState } from 'react';
import fetchUser from '../api/rest/fetch-user';

function useUser(userId) {
  const [user, setUser] = useState(null);

  const updateUser = useCallback(async () => {
    const response = await fetchUser(String(userId));
    const { data } = response;
    setUser(data);
  }, [setUser, userId]);

  useEffect(() => {
    if (userId != null) {
      updateUser();
    }
  }, [userId, updateUser]);

  return user;
}

export default useUser;
