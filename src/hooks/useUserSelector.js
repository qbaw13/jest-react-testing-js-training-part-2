import { useCallback, useState } from 'react';
import useUser from './useUser';

const DEFAULT_USER_ID = 1;

function useUserSelector(initialUserId = DEFAULT_USER_ID) {
  const [userId, setUserId] = useState(initialUserId);
  const user = useUser(userId);

  const resetUser = useCallback(() => {
    setUserId(initialUserId);
  }, [initialUserId, setUserId]);

  const randomUser = useCallback(() => {
    setUserId(Math.floor(Math.random() * 100));
  }, [setUserId]);

  const selectUserById = useCallback(
    (id) => {
      if (id < 1 || id > 100) {
        return;
      }
      setUserId(id);
    },
    [setUserId]
  );

  return {
    user,
    userId,
    resetUser,
    randomUser,
    selectUserById,
  };
}

export default useUserSelector;
