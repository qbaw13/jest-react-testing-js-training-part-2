import { memo, useState, useContext, useEffect } from 'react';
import { compose } from 'recompose';
import { useForm, Controller } from 'react-hook-form';
import CircularProgress from '@mui/material/CircularProgress';
import List from '@mui/material/List';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import withGrowthRateProvider from './hocs/withGrowthRateProvider';
import GrowthRateContext from '../../contexts/GrowthRateContext';
import {
  MAX_GROWTH_RATE,
  MAX_NUMBER_OF_YEARS,
  MIN_AMOUNT,
  MIN_GROWTH_RATE,
  MIN_NUMBER_OF_YEARS,
} from '../../constants/investment-details';
import { collectInvestmentDetails } from '../../services/investment';
import useDefaultFormValues from './hooks/useDefaultFormValues';
import InvestmentListItem from '../InvestmentListItem/InvestmentListItem';
import withPremiumAccountNotification from '../../hocs/withPremiumAccountNotification';
import useUser from '../../hooks/useUser';

function InvestmentPanel({ activeUserId }) {
  const user = useUser(activeUserId);
  const { growthRate, loading } = useContext(GrowthRateContext);
  const defaultValues = useDefaultFormValues({ growthRate });
  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({ defaultValues });
  const [invesmentDetails, setInvestmentDetails] = useState({});
  const { annualGrowth } = invesmentDetails;

  useEffect(() => {
    reset(defaultValues);
  }, [reset, defaultValues]);

  const onSubmit = (data) => {
    const { amount, numberOfYears, growthRate } = data;
    const investmentDetails = collectInvestmentDetails({
      amount: Number(amount),
      numberOfYears: Number(numberOfYears),
      growthRate: Number(growthRate),
    });
    setInvestmentDetails(investmentDetails);
  };

  if (loading) {
    return <CircularProgress />;
  }

  return (
    <div>
      <header>
        <Typography variant="h3" component="h1">
          Investment panel
        </Typography>
        {user != null ? (
          <Typography variant="subtitle1">
            Hi, {user?.name} {user?.surname}
          </Typography>
        ) : null}
      </header>
      <section>
        <Typography variant="h4" component="h2">
          Input
        </Typography>
        <form onSubmit={handleSubmit(onSubmit)} aria-label="form">
          <Controller
            name="growthRate"
            control={control}
            defaultValue=""
            rules={{ min: MIN_GROWTH_RATE, max: MAX_GROWTH_RATE }}
            render={({ field }) => (
              <TextField
                {...field}
                id="Growth rate"
                label="Growth rate"
                type="number"
                variant="outlined"
                margin="normal"
                error={Boolean(errors.growthRate)}
                helperText={
                  Boolean(errors.growthRate) &&
                  `Input value should be between ${MIN_GROWTH_RATE} and ${MAX_GROWTH_RATE}`
                }
                fullWidth
              />
            )}
          />
          <Controller
            name="amount"
            control={control}
            defaultValue=""
            rules={{ min: MIN_AMOUNT }}
            render={({ field }) => (
              <TextField
                {...field}
                id="Amount"
                label="Amount"
                type="number"
                variant="outlined"
                margin="normal"
                error={Boolean(errors.amount)}
                helperText={
                  Boolean(errors.amount) &&
                  `Input value should be greater or equal ${MIN_AMOUNT}`
                }
                fullWidth
              />
            )}
          />
          <Controller
            name="numberOfYears"
            control={control}
            defaultValue=""
            rules={{ min: MIN_NUMBER_OF_YEARS, max: MAX_NUMBER_OF_YEARS }}
            render={({ field }) => (
              <TextField
                {...field}
                id="Number of years"
                label="Number of years"
                type="number"
                variant="outlined"
                margin="normal"
                error={Boolean(errors.numberOfYears)}
                helperText={`Input value between ${MIN_NUMBER_OF_YEARS} and ${MAX_NUMBER_OF_YEARS}`}
                fullWidth
              />
            )}
          />
          <Button color="primary" variant="contained" type="submit" fullWidth>
            Calculate
          </Button>
        </form>
      </section>
      <Typography variant="h4" component="h2">
        Investment
      </Typography>
      <section>
        {annualGrowth ? (
          <List>
            {annualGrowth.map((amount, index) => (
              <InvestmentListItem key={amount} year={index} amount={amount} />
            ))}
          </List>
        ) : (
          <Typography variant="body1">
            Please fill in investment form
          </Typography>
        )}
      </section>
    </div>
  );
}

const EnhancedInvestmentPanel = compose(
  withGrowthRateProvider,
  withPremiumAccountNotification,
  memo
)(InvestmentPanel);

export default EnhancedInvestmentPanel;
