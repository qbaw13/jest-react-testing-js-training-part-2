import { GrowthRateProvider } from '../../../contexts/GrowthRateContext';

function withGrowthRateProvider(WrappedComponent) {
  return function Wrapper(props) {
    return (
      <GrowthRateProvider>
        <WrappedComponent {...props} />
      </GrowthRateProvider>
    );
  };
}

export default withGrowthRateProvider;
