import { render, screen, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { MockedProvider } from '@apollo/client/testing';
import InvestmentPanel from './InvestmentPanel';
import FETCH_GROWTH_RATE from '../../api/graphql/fetch-growth-rate';

jest.mock('../../hocs/withPremiumAccountNotification', () => ({
  __esModule: true,
  default: (Component) => (props) => <Component {...props} />,
}));

jest.mock('../../api/rest/fetch-user', () => ({
  __esModule: true,
  default: () =>
    Promise.resolve({
      data: { name: 'NAME', surname: 'SURNAME' },
    }),
}));

const DEFAULT_PROPS = {
  userId: null,
};

function createSut(props) {
  const mocks = [
    {
      request: {
        query: FETCH_GROWTH_RATE,
        variables: { id: 1 },
      },
      result: {
        data: {
          rate: {
            id: 134513,
            percentageRate: 53,
          },
        },
      },
    },
  ];

  return render(
    <MockedProvider mocks={mocks} addTypename={false}>
      <InvestmentPanel {...DEFAULT_PROPS} {...props} />
    </MockedProvider>
  );
}

async function waitForRender() {
  await screen.findByText('Investment panel');
}

describe('InvestmentPanel', () => {
  it('should render loading indicator', async () => {
    // when
    createSut();

    // then
    expect(screen.getByRole('progressbar')).toBeInTheDocument();
    await waitForRender();
  });

  it('should render user name when is successfuly fetched', async () => {
    // given
    const props = { activeUserId: 'SOME_USER_ID' };

    // when
    createSut(props);
    await waitForRender();

    // then
    expect(screen.getByText(/Hi, NAME SURNAME/)).toBeInTheDocument();
  });

  it('should NOT render user name when user id is null', async () => {
    // given
    const props = { activeUserId: null };

    // when
    createSut(props);
    await waitForRender();

    // then
    expect(screen.queryByText(/Hi,.*/i)).not.toBeInTheDocument();
  });

  it('should render investment annual growth', async () => {
    // given
    createSut();
    await waitForRender();
    userEvent.type(screen.getByLabelText('Amount'), '312');
    userEvent.type(screen.getByLabelText('Number of years'), '3');

    // when
    fireEvent.submit(screen.getByRole('form'));

    // then
    const listItems = await screen.findAllByRole('listitem');
    listItems.forEach((listItem, index) => {
      expect(listItem).toHaveTextContent(
        new RegExp(`.*year ${index}[0-9]+.*`, 'i')
      );
    });
  });

  it.each`
    amount    | numberOfYears
    ${''}     | ${'1'}
    ${'1'}    | ${''}
    ${'2341'} | ${'0'}
    ${'5345'} | ${'-1'}
    ${'7543'} | ${'51'}
    ${'0'}    | ${'1'}
    ${'-1'}   | ${'1'}
  `(
    'should inform user when at least one of the inputs is empty or invalid',
    async ({ amount, numberOfYears }) => {
      // given
      createSut();
      await waitForRender();

      // when
      userEvent.type(screen.getByLabelText('Amount'), amount);
      userEvent.type(screen.getByLabelText('Number of years'), numberOfYears);
      fireEvent.submit(screen.getByRole('form'));

      // then
      expect(
        await screen.findByText('Please fill in investment form')
      ).toBeInTheDocument();
    }
  );

  it('should highlight error when numberOfYears is invalid', async () => {
    // given
    const numberOfYears = '423523';
    createSut();
    await waitForRender();

    // when
    userEvent.type(screen.getByLabelText('Number of years'), numberOfYears);
    fireEvent.submit(screen.getByRole('form'));

    // then
    expect(await screen.findByText('Input value between 1 and 50')).toHaveClass(
      'Mui-error'
    );
  });
});
