import { useMemo } from 'react';

const EMPTY_VALUE = '';

export default function useDefaultFormValues({ growthRate }) {
  const defaultValues = useMemo(
    () => ({
      growthRate: growthRate == null ? EMPTY_VALUE : growthRate,
      amount: EMPTY_VALUE,
      numberOfYears: EMPTY_VALUE,
    }),
    [growthRate]
  );

  return defaultValues;
}
