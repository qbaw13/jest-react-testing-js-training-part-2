import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Stack from '@mui/material/Stack';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import useUserSelector from '../../hooks/useUserSelector';

function Users() {
  const { user, userId, resetUser, selectUserById, randomUser } =
    useUserSelector();

  const handlePrevUserClick = () => {
    selectUserById(userId - 1);
  };

  const handleNextUserClick = () => {
    selectUserById(userId + 1);
  };

  return (
    <div>
      <header>
        <Typography variant="h3" component="h1">
          Users
        </Typography>
      </header>
      <List>
        <ListItem>
          <ListItemText>id: {user?.id}</ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            name: {user?.name} {user?.surname}
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>premium: {user?.premium ? 'YES' : 'NO'}</ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>createdAt: {user?.createdAt}</ListItemText>
        </ListItem>
      </List>
      <Stack spacing={2} direction="row">
        <Button color="secondary" variant="contained" onClick={resetUser}>
          Reset user
        </Button>
        <Button
          color="secondary"
          variant="contained"
          onClick={handlePrevUserClick}
        >
          Previous user
        </Button>
        <Button
          color="secondary"
          variant="contained"
          onClick={handleNextUserClick}
        >
          Next user
        </Button>
        <Button color="secondary" variant="contained" onClick={randomUser}>
          Random user
        </Button>
      </Stack>
    </div>
  );
}

export default Users;
