import { memo } from 'react';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';

function InvestmentListItem({ year, amount }) {
  return (
    <ListItem>
      <ListItemText>year {year}</ListItemText>
      <ListItemText>{amount}</ListItemText>
    </ListItem>
  );
}

export default memo(InvestmentListItem);
