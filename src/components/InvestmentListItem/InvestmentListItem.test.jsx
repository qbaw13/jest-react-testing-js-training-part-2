import { render, screen } from "@testing-library/react";
import InvestmentListItem from "./InvestmentListItem";

function createSut(props) {
  return render(<InvestmentListItem {...props} />);
}

describe("InvestmentListItem", () => {
  it("should list a year", () => {
    // given
    const props = {
      year: 423234,
      amount: 5435423,
    };

    // when
    createSut(props);

    // then
    expect(screen.getByText(/year.*/).textContent).toMatchInlineSnapshot(
      `"year 423234"`
    );
  });

  it("should list an amount", () => {
    // given
    const props = {
      year: 423234,
      amount: 5435423,
    };

    // when
    createSut(props);
    // then
    expect(screen.getByText(/^\d+$/i).textContent).toMatchInlineSnapshot(
      `"5435423"`
    );
  });
});
