import { BrowserRouter, Switch, Route } from 'react-router-dom';
import InvestmentPanel from './InvestmentPanel/InvestmentPanel';
import Users from './Users/Users';
import useActiveUserId from '../hooks/useActiveUserId';

function App() {
  const activeUserId = useActiveUserId();

  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path="/">
            <InvestmentPanel activeUserId={activeUserId} />
          </Route>
          <Route path="/users">
            <Users />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
