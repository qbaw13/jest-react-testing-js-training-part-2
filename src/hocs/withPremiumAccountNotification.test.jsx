import {
  act,
  render,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import withPremiumAccountNotification from './withPremiumAccountNotification';
import fetchUserMock from '../api/rest/fetch-user';

jest.mock('../api/rest/fetch-user');

const DEFAULT_USER = {
  id: 'ID',
  name: 'NAME',
  surname: 'SURNAME',
  createdAt: 'CREATED_AT',
  premium: false,
};

const StubComponent = (props) => {
  return (
    <div>
      <span>COMPONENT</span>
      <span data-testid="props">{JSON.stringify(props)}</span>
    </div>
  );
};

function createSut({ user = DEFAULT_USER, props }) {
  fetchUserMock.mockReturnValueOnce(
    Promise.resolve({
      data: {
        ...DEFAULT_USER,
        premium: user.premium,
      },
    })
  );

  const EnhancedComponent = withPremiumAccountNotification(StubComponent);
  return render(<EnhancedComponent {...props} />);
}

async function waitForRender() {
  await screen.findByText('COMPONENT');
}

describe('premium account notification', () => {
  it('should pass all props to the wrapped component', async () => {
    // given
    const props = {
      PROP_1: 'PROP_1',
      PROP_2: 'PROP_2',
    };

    // when
    createSut({ props });
    await waitForRender();

    // then
    expect(screen.getByTestId('props')).toHaveTextContent(
      JSON.stringify(props)
    );
  });

  it('should render notification when user has not got premium account', async () => {
    // given
    const user = {
      premium: false,
    };

    // when
    createSut({ user });
    await waitForRender();

    // then
    expect(screen.getByRole('dialog')).toBeInTheDocument();
  });

  it('should NOT render notification when user has premium account', async () => {
    // given
    const user = {
      premium: true,
    };

    // when
    createSut({ user });
    await waitForRender();

    // then
    expect(screen.queryByRole('dialog')).not.toBeInTheDocument();
  });

  it('should close dialog on cancel click', async () => {
    // given
    const user = {
      premium: false,
    };
    createSut({ user });
    await waitForRender();

    // when
    act(() => {
      userEvent.click(screen.getByText('Cancel'));
    });
    await waitForElementToBeRemoved(() => screen.queryByRole('dialog'));

    // then
    expect(screen.queryByRole('dialog')).not.toBeInTheDocument();
  });
});
