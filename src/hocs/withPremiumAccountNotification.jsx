import { useState } from 'react';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';
import useUser from '../hooks/useUser';
import upgradeAccount from '../api/rest/upgrade-account';
import useActiveUserId from '../hooks/useActiveUserId';

function withPremiumAccountNotification(WrappedComponent) {
  return function Wrapper(props) {
    const activeUserId = useActiveUserId();
    const user = useUser(activeUserId);
    const [open, setOpen] = useState(true);

    const handleUpgradeClick = () => {
      upgradeAccount();
      setOpen(false);
    };

    const handleCloseClick = () => {
      setOpen(false);
    };

    if (user == null || user.premium) {
      return <WrappedComponent {...props} />;
    }

    return (
      <>
        <Dialog open={open} onClose={handleCloseClick}>
          <DialogTitle>Account plan</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Upgrade your account to premium
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseClick} color="primary">
              Cancel
            </Button>
            <Button onClick={handleUpgradeClick} color="primary" autoFocus>
              Upgrade
            </Button>
          </DialogActions>
        </Dialog>
        <WrappedComponent {...props} />
      </>
    );
  };
}

export default withPremiumAccountNotification;
