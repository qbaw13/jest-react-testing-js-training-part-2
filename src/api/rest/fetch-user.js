import axios from 'axios';

const url = 'https://mockend.com/qbaw13/test/users/';

async function fetchUser(id) {
  return axios.get(`${url}${id}`);
}

export default fetchUser;
