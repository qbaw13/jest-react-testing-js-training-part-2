import axios from 'axios';

const url = 'https://mockend.com/qbaw13/test/rates/1';

async function fetchGrowthRate() {
  return axios.get(url);
}

export default fetchGrowthRate;
