import { useQuery } from '@apollo/client';
import { createContext, useMemo } from 'react';
import FETCH_GROWTH_RATE from '../api/graphql/fetch-growth-rate';

const GROWTH_RATE_ID = 1;

const GrowthRateContext = createContext({
  loading: false,
  growthRate: null,
});

const normalizeGrowthRate = (percentageGrowthRate) =>
  percentageGrowthRate / 100;

export function GrowthRateProvider({ children }) {
  const { loading, error, data } = useQuery(FETCH_GROWTH_RATE, {
    variables: {
      id: GROWTH_RATE_ID,
    },
  });

  const growthRate = useMemo(() => {
    if (!data?.rate) {
      return null;
    }
    const {
      rate: { percentageRate },
    } = data;
    return normalizeGrowthRate(percentageRate);
  }, [data]);

  const value = useMemo(
    () => ({
      loading,
      error,
      growthRate,
    }),
    [loading, error, growthRate]
  );

  return (
    <GrowthRateContext.Provider value={value}>
      {children}
    </GrowthRateContext.Provider>
  );
}

export default GrowthRateContext;
