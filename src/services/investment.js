import { MAX_NUMBER_OF_YEARS } from '../constants/investment-details';
import calculateAnnualGrowth from '../utils/calculate-annual-growth';

function collectAnnualGrowth(amount, numberOfYears, growthRate) {
  if (
    amount == null ||
    amount <= 0 ||
    numberOfYears == null ||
    numberOfYears <= 0 ||
    numberOfYears > MAX_NUMBER_OF_YEARS ||
    growthRate == null
  ) {
    return;
  }
  return calculateAnnualGrowth(amount, numberOfYears, growthRate);
}

export function collectInvestmentDetails({
  amount,
  numberOfYears,
  growthRate,
}) {
  const annualGrowth = collectAnnualGrowth(amount, numberOfYears, growthRate);

  return {
    annualGrowth,
  };
}
